//
//  AI_ChatApp.swift
//  AI-Chat
//
//  Created by Bagrat Arutyunov on 11.12.2023.
//

import SwiftUI

@main
struct AI_ChatApp: App {
    @ObservedObject var appState = AppState()
    var body: some Scene {
        WindowGroup {
            if appState.isLoggedIn {
                NavigationStack(path: $appState.navigationPath) {
                    ChatListView()
                        .environmentObject(appState)
                }
            } else {
                AuthView()
                    .environmentObject(appState)
            }
        }
    }
}
