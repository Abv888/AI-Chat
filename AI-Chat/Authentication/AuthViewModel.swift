//
//  AuthViewModel.swift
//  AI-Chat
//
//  Created by Bagrat Arutyunov on 11.12.2023.
//

import Foundation

class AuthViewModel: ObservableObject {
    @Published var emailText = ""
    @Published var passwordText = ""
    
    @Published var isLoading = false
    @Published var isPasswordVisible = false
    @Published var userExists = false
    
    let authService = AuthService()
    
    func authenticate(_ state: AppState) {
        isLoading = true
        Task {
            do {
                if isPasswordVisible {
                    let result = try await authService.login(email: emailText, password: passwordText, userExists: userExists)
                    await MainActor.run {
                        guard let result = result else { return }
                        state.currentUser = result.user
                    }
                } else {
                    userExists = try await authService.checkUserExists(email: emailText)
                    isPasswordVisible = true
                }
                isLoading = false
            } catch {
                print(error.localizedDescription)
                await MainActor.run {
                    isLoading = false
                }
            }
        }
    }
}
