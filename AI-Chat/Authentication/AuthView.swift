//
//  AuthView.swift
//  AI-Chat
//
//  Created by Bagrat Arutyunov on 11.12.2023.
//

import SwiftUI

struct AuthView: View {
    @ObservedObject var viewModel = AuthViewModel()
    @EnvironmentObject var appState: AppState
    
    var body: some View {
        VStack {
            Text("ChatGPT iOS App")
                .modifier(Title())
            TextField("Email", text: $viewModel.emailText)
                .modifier(AuthTextField())
            if viewModel.isPasswordVisible {
                SecureField("Password", text: $viewModel.passwordText)
                    .modifier(AuthTextField())
            }
            if viewModel.isLoading {
                ProgressView()
            } else {
                Button {
                    viewModel.authenticate(appState)
                } label: {
                    Spacer()
                    Text(viewModel.userExists ? "Log In" : "Sign Up")
                    Spacer()
                }
                .modifier(LogInButton())
            }
        }
        .padding()
    }
}

#Preview {
    AuthView()
}
