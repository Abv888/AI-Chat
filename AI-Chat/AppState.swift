//
//  AppState.swift
//  AI-Chat
//
//  Created by Bagrat Arutyunov on 12.12.2023.
//

import Foundation
import FirebaseCore
import SwiftUI
import Firebase

class AppState: ObservableObject {
    
    @Published var currentUser: User?
    @Published var navigationPath = NavigationPath()
    
    var isLoggedIn: Bool {
        return currentUser != nil
    }
    
    init() {
        FirebaseApp.configure()
        
        if let currentUser = Auth.auth().currentUser {
            self.currentUser = currentUser
        }
    }
}
