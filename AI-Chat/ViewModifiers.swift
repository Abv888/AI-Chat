//
//  ViewModifiers.swift
//  AI-Chat
//
//  Created by Bagrat Arutyunov on 12.12.2023.
//

import SwiftUI

struct AuthTextField: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .background(Color.gray.opacity(0.1))
            .textInputAutocapitalization(.never)
            .clipShape(RoundedRectangle(cornerRadius: 12))
    }
}

struct Title: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.title)
            .bold()
    }
}

struct LogInButton: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .foregroundStyle(.white)
            .background(Color.blue)
            .clipShape(RoundedRectangle(cornerRadius: 12, style: .continuous))
    }
}
